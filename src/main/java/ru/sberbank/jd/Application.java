package ru.sberbank.jd;

import ru.sberbank.jd.dto.*;

public class Application {
    public static void main(String[] args) {
        System.out.println("Hello");

        Land land = new Land("26:29:0000:12");
        land.setAppraisedValue(500000.00);
        land.setMortgageValue(250000.00);
        land.setAdress("улица Цветочная, 10");
        land.setArea(10);
        land.setDestination("ИЖС");

        Home home = new Home();
        home.setCadNum("df:12:111:23");
        home.setAppraisedValue(1000000.00);
        home.setMortgageValue(500000.00);
        home.setAdress("улица Цветочная, 10");
        home.setArea(120.12);

        land.GetInfo();
        home.GetInfo();
    }
}
