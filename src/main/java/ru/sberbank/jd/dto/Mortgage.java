package ru.sberbank.jd.dto;

import lombok.Data;
import lombok.Getter;

@Data
public abstract class Mortgage {
    /*
        Залоговое имущество
     */
    public final static String typeMortgage = "Корень Залоговое имущество"; // Тип объекта залога
    private Double appraisedValue;      // Оценочная стоимость
    private Double mortgageValue;       // Залоговая стоимость
    private String adress;              // Месторасположение

    public Mortgage() {
        this.appraisedValue= null;
        this.mortgageValue= null;
        this.adress= "";
    }

    public void GetInfo() {
        System.out.println("Описание не назначено");
    }

}



