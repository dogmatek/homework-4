package ru.sberbank.jd.dto;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class RealtyTest {
    @Test
    public void test1() {
        String cadnum = "24:12:12222:12";
        Home homeTest = new Home(cadnum);
        Assert.assertEquals(cadnum, homeTest.getCadNum());
    }

    @Test
    public void test2() {
        String cadnum = "24:34:223232:12";
        Home homeTest = new Home(cadnum);
        Assert.assertEquals(cadnum, homeTest.getCadNum());
    }
}