package ru.sberbank.jd.dto;

import lombok.Data;
import lombok.Getter;

@Data
public final class Land extends Realty {
    /*
        Земельный участок
     */
    private Integer area;        // Площадь
    private String destination;  // Назначение


    public Land(){
        area = null;
        destination = null;
    }

    public Land( String cadnum ){
        super(cadnum);
        area = null;
        destination = null;
    }

    public void GetInfo() {
        System.out.println("\n--------------------------------------------");
        System.out.println("Описание залога земельного участка");
        System.out.println("Кадастровый номер: "+ this.getCadNum());
        System.out.println("Площадь: " + this.getArea() + " кв.м");
        System.out.println("Назначение: " + this.getArea());
        System.out.println("Оценочная стоимость: " + this.getAppraisedValue());
        System.out.println("Залоговая стоимость: " + this.getMortgageValue());
        System.out.println("--------------------------------------------\n");
    }

}
