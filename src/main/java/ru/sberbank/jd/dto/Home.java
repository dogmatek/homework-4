package ru.sberbank.jd.dto;


import lombok.Data;

@Data
public final class Home extends Realty {
    /*
        Жилой дом
     */
    private Double area;         // Площадь
    private Integer floors;      // Этажность
    private boolean Gas;         // Наличие газоснабжения

    public Home(  ){
        area = null;
        floors = null;
        Gas = false;
    }

    public Home( String cadnum ){
        super(cadnum);
        area = null;
        floors = null;
        Gas = false;

    }

    public void GetInfo() {
        System.out.println("\n--------------------------------------------");
        System.out.println("Описание залога Жилого дома");
        System.out.println("Кадастровый номер: "+ this.getCadNum());
        System.out.println("Площадь: " + this.getArea() + " кв.м");
        System.out.println("Этажность: " + this.getFloors());
        if (this.isGas()) System.out.println("Имеется газоснобжение" );
        System.out.println("Оценочная стоимость: " + this.getAppraisedValue());
        System.out.println("Залоговая стоимость: " + this.getMortgageValue());
        System.out.println("--------------------------------------------\n");
    }

}
