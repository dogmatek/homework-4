package ru.sberbank.jd.dto;

import lombok.Getter;

import java.util.regex.Pattern;

@Getter
public abstract class Realty extends Mortgage {
    /*
        Недвижимое имущество
     */
    private String CadNum;

    Realty() {
        this.CadNum = null;
    }

    Realty(String cadNum) {
        if ( validateCadNum(cadNum) == true){
            this.CadNum = cadNum;
        }
    }


    public void setCadNum(String cadNum) {
        if ( validateCadNum(cadNum) == true){
            this.CadNum = cadNum;
        }
    }

    // Валидация кадастрового номера
    private static boolean validateCadNum(String cadNum){

        if(!Pattern.matches("\\d\\d:\\d\\d:\\d{1,10}:\\d{1,10}",cadNum)) {
            System.out.println("Не верно указан кадастровый номер: "+ cadNum);
            return false;
        }
        else {
            System.out.println("Кадастровый номер соответствует шаблону");
            return true;
        }
    }

}
